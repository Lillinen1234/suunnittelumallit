package factorymethod;

public class Tutkintovastaava extends AterioivaOtus{

    public Juoma createJuoma(){
        return new Maito();
    };
    public Ruoka createRuoka() { return new Makkarakeitto();}
}
