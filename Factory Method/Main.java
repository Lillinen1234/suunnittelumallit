package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus opiskelija = new Opiskelija();
        AterioivaOtus tutkintovastaava = new Tutkintovastaava();
        opettaja.aterioi();
        opiskelija.aterioi();
        tutkintovastaava.aterioi();
    }
}
